# @field DEVICENAME
# @type STRING
# Device name, should be the same as the name in CCDB

# @field CONTROLLERNAME
# @type STRING
# Name of the head unit


#Load the database defining your EPICS records
dbLoadRecords(vac_pump_digitelqpce_vpi.db, "P = $(DEVICENAME), R = :, CONTROLLERNAME = $(CONTROLLERNAME), CHANNEL = 3, nDO = 7, ASYNPORT = $(CONTROLLERNAME)-asyn-port")
