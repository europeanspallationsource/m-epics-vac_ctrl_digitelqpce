# @field DEVICENAME
# @type STRING
# Device name, should be the same as the name in CCDB

# @field CONTROLLERNAME
# @type STRING
# Name of the head unit

# @field CHANNEL
# @type INTEGER
# Channel on the head unit where the pump is connected to (1, 2, 3, 4)


requireSnippet(_vac_pump_digitelqpce_vpi_ch${CHANNEL}.cmd, "DEVICENAME = $(DEVICENAME), CONTROLLERNAME = $(CONTROLLERNAME)")
