# Ion Pump Controller Digitel QPCe

EPICS module to provide communications and read/write data from/to Digitel QPC ion pump power supply controller

## Startup Examples

`iocsh -r vac_ctrl_digitelqpce,2.0.0 -c 'requireSnippet(vac_ctrl_digitelqpce_ethernet.cmd, "DEVICENAME=MEBT-010:Vac-VEPI-02100, IPADDR=10.4.0.213, PORT=4002)'`

If ran as proper ioc service:
```
epicsEnvSet(DEVICENAME, "MEBT-010:Vac-VEPI-02100")
epicsEnvSet(IPADDR, "10.10.1.21")
epicsEnvSet(PORT, "4002")
require vac_ctrl_digitelqpce, 2.0.0
< ${REQUIRE_vac_ctrl_digitelqpce_PATH}/startup/vac_ctrl_digitelqpce_ethernet.cmd
```
